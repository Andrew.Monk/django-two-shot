from django.urls import path
from receipts.views import receipts, create_receipt, category_list, account_list, create_category


urlpatterns = [
    path('', receipts, name='home'),
    path('create/', create_receipt, name='create_receipt'),
    path('category/', category_list, name='category_list'),
    path('account/', account_list, name='account_list'),
    path('categories/create', create_category, name='create_category')
]
