from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import RecieptForm, ExpenseCategoryForm


@login_required
def receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = RecieptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = RecieptForm()

    context = {
        "form": form
        }
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "receipts": categories
        }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account": account
        }
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, 'receipts/create_category.html', context)
