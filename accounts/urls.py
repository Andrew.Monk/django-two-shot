from django.urls import path
from accounts.views import login_view, logout_view, signup, create_account

urlpatterns = [
    path('create/', create_account, name='create_account'),
    path('signup/', signup, name='signup'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
]
